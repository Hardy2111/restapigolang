package server

import (
	"github.com/gin-gonic/gin"
	"service/handler"
)

type ApiServer struct {
	H      *handler.Hand
	Engine *gin.Engine
}

func InitServer(h *handler.Hand) ApiServer {
	router := gin.Default()
	router.GET("/handle/:id", h.GetObjectById)
	router.POST("/handle", h.AddObject)
	return ApiServer{h, router}
}

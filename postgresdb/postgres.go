package postgresdb

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"log"
)

const (
	USERS_TABLE    = "postgres"
	MESSAGES_TABLE = "messages"
	PRODUCTS_TABLE = "products"
)

type Config struct {
	Host     string
	Port     string
	Username string
	Password string
	DBName   string
	SSLMode  string
}

func NewPostgresDB(cfg Config) (*sqlx.DB, error) {
	db, err := sqlx.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
		cfg.Host, cfg.Port, cfg.Username, cfg.DBName, cfg.Password, cfg.SSLMode))
	if err != nil {
		log.Fatalln("Bad open sqlx", err)
	}

	err = db.Ping()
	if err != nil {
		log.Fatalln("Bad connect to DB", err)
	}

	return db, nil
}

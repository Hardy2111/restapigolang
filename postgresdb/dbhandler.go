package postgresdb

import (
	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"
	"log"
	"os"
)

func InitDB() *sqlx.DB {
	sqlxDb, err := NewPostgresDB(Config{
		Host:     viper.GetString("db.host"),
		Port:     viper.GetString("db.port"),
		Username: viper.GetString("db.username"),
		DBName:   viper.GetString("db.dbname"),
		SSLMode:  viper.GetString("db.sslmode"),
		Password: os.Getenv("DB_PASSWORD"),
	})

	if err != nil {
		log.Fatalln("postgres", err)
	}

	return sqlxDb
}

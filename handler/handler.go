package handler

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"io"
	"log"
	"net/http"
	"service/model"
	"service/postgresdb"
)

type Hand struct {
	Db    *sqlx.DB
	Cache map[interface{}]string
}

func (h *Hand) GetObjectById(c *gin.Context) {
	id := c.Param("id")
	if val, ok := h.Cache[id]; ok {
		c.IndentedJSON(http.StatusFound, val)
		return
	}
	c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Id not found"})
}

func (h *Hand) AddObject(c *gin.Context) {

	all, err := io.ReadAll(c.Request.Body)
	if err != nil {
		log.Fatalln("Bad Read!", err)
	}

	var jsonMap map[string]interface{}
	err = json.Unmarshal(all, &jsonMap)
	if err != nil {
		log.Fatalln("Bad Unmarshal", err)
	}

	defer c.Request.Body.Close()

	if _, ok := jsonMap["order_uid"]; !ok {
		c.IndentedJSON(http.StatusNotFound, gin.H{"Status": "Bad"})
		return
	}

	id := jsonMap["order_uid"]
	query := fmt.Sprintf("INSERT INTO %s (order_uid, json_obj) VALUES ($1, $2)", postgresdb.PRODUCTS_TABLE)
	row := h.Db.QueryRow(query, id, all)
	if row.Err() != nil {
		log.Fatalln("Bad query!", row.Err())
	}

	h.Cache[id] = string(all)

	c.IndentedJSON(http.StatusCreated, gin.H{"Status": "Ok"})
}

func (h *Hand) SyncCacheWithDB() {
	query := fmt.Sprintf("SELECT * FROM %s", postgresdb.PRODUCTS_TABLE)
	rows, err := h.Db.Query(query)
	if err != nil || rows.Err() != nil {
		log.Fatalln("SyncCacheWithDB", err, rows.Err())
	}

	var row model.Row
	for rows.Next() {
		err = rows.Scan(&row.Id, &row.OrderUid, &row.Data)
		if err != nil {
			log.Fatal(err)
		}
		h.Cache[row.OrderUid] = row.Data
	}
}

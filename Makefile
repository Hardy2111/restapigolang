postgres:
	docker run --name dbwb -e POSTGRES_PASSWORD='qwerty' -d --rm -p5432:5432 postgres

updb:
	migrate -path ./schema -database 'postgres://postgres:qwerty@localhost:5432/postgres?sslmode=disable' up

downdb:
	migrate -path ./schema -database 'postgres://postgres:qwerty@localhost:5432/postgres?sslmode=disable' down

nats-streaming:
	docker run --rm -ti -d -p4222:4222 nats-streaming

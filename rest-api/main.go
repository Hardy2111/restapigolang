package main

import (
	"github.com/joho/godotenv"
	"github.com/spf13/viper"
	"log"
	"service/handler"
	"service/postgresdb"
	"service/server"
	"service/stan/sub"
)

// Run before main for add configs
func init() {
	viper.AddConfigPath("configs")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalln("Bad ReadInConfig", err)
	}

	err = godotenv.Load(".env")
	if err != nil {
		log.Fatalln("Bad godotenv", err)
	}
}

func main() {

	//Connect to db
	h := handler.Hand{postgresdb.InitDB(), map[interface{}]string{}}
	//Create gin.Engine
	s := server.InitServer(&h)
	h.SyncCacheWithDB()

	go sub.Subscribe()

	err := s.Engine.Run("localhost:" + viper.GetString("port"))
	if err != nil {
		log.Fatalln("Run error!")
	}

}
